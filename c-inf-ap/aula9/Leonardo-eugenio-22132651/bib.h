#ifndef BIB_H_INCLUDED
#define BIB_H_INCLUDED
#endif

#include<stdlib.h>
#include<stdio.h>
#include<math.h>

float getRe(float parametros[4]){
    return (parametros[0]*parametros[1]*parametros[2]/parametros[3]);
}

float getF(float Re){
    if(Re<2300)
        return (64.0/Re);
    else
        return (0.316/pow(Re,.25));
}