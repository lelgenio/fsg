#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include"bib.h"

int main()
{
    float Re, f, parametros[4]; // Vm p D µ
    for (int i = 0; i < 4; i++)
        scanf("%f", &parametros[i]);

    Re = getRe(parametros);
    f  = getF(Re);

    for (int i = 0; i < 4; i++)
        printf("parametro %d = %.2f\n", i+1, parametros[i]);
    printf("Re = %.2f\n", Re);
    printf("f  = %.2f\n", f);
    
    return 0;
}
