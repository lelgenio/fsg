#include<stdlib.h>
#include<stdio.h>
#include<math.h>

#define SIZE  3

int main()
{

    float A[SIZE][SIZE]={
        {1,0,0},
        {0,1,0},
        {0,0,1}
    }; 
    float B[SIZE][SIZE]={
        {1,0,0},
        {0,1,0},
        {0,0,1}
    }; 

    float res[SIZE][SIZE];
    for (int i = 0; i < SIZE; i++)
        for (int j = 0; j < SIZE; j++)
            res[i][j]=0;
    
    for (int i = 0; i < SIZE; i++)
        for (int j = 0; j < SIZE; j++)
            for (int k = 0; k < SIZE; k++)
                res[i][j] += A[i][k] * B[k][j];
        
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
            printf("%.2f ", res[i][j]); 
        printf("\n"); 
    }
    
    return 0;
}
