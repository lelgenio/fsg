#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char const *argv[])
{
    int a[5], b[5], c[5],
        somaQuad[3] = {0, 0, 0};
    for (int i = 0; i < 5; i++)
    {
        somaQuad[0] += pow(a[i], 2);
        somaQuad[1] += pow(b[i], 2);
        somaQuad[2] += pow(c[i], 2);
    }
    somaQuad[0] = sqrt(somaQuad[0]);
    somaQuad[1] = sqrt(somaQuad[1]);
    somaQuad[2] = sqrt(somaQuad[2]);

    printf("modA=%d modA=%d modA=%d ",
           somaQuad[0], somaQuad[1], somaQuad[2]);

    return 0;
}
