#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int main(int argc, char const *argv[])
{
    float m = 10, a[3] = {0, 1, 0}, f[3], fMod;
    int fSum = 0;
    for (int i = 0; i < 3; i++)
    {
        f[i] = m * a[i];
        fSum += pow(f[i], 2);
    }
    printf("f.x = %.2f\n", f[0]);
    printf("f.y = %.2f\n", f[1]);
    printf("f.z = %.2f\n", f[2]);
    printf("f.m = %.2f\n", sqrt(fSum));

    return 0;
}
