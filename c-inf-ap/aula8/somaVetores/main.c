#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char const *argv[])
{
    float a[5], b[5], c[5];
    for (int i = 0; i < 5; i++)
    {
        printf("a[%d] = ", i);
        scanf("%f", &a[i]);
    }
    for (int i = 0; i < 5; i++)
    {
        printf("b[%d] = ", i);
        scanf("%f", &b[i]);
    }
    for (int i = 0; i < 5; i++)
    {
        c[i] = a[i] + b[i];
        printf("c[%d] = %.2f\n", i, c[i]);
    }

    return 0;
}
