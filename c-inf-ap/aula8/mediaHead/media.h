#ifndef MEDIA_H_INCLUDED
#define MEDIA_H_INCLUDED
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
float calcMedia(int quantNum)
{
    float media = 0, entrada;
    for (int i = 0; i < quantNum; i++)
    {
        printf("#%d = ", i);
        scanf("%f", &entrada);
        media += entrada;
    }
    return media / (float)quantNum;
}