#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int i;
    float notas[5];
    for (int i = 0; i < 5; i++)
    {
        printf("Digite a nota do aluno %d: ", i + 1);
        scanf("%f", &notas[i]);
    }

    float med = 0;
    for (int i = 0; i < 5; i++)
        med += notas[i];
    med /= 5;
    printf("A media é %.2f: ", med);
    return 0;
}
