#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include"bib.h"

#define SIZE 3

int main()
{
    float res[SIZE],
    A[SIZE][SIZE]={
        {3,2,1},
        {5,6,4},
        {1,2,3}
    }, 
    a[SIZE]={
        5,
        2,
        1
    };

    for (int i = 0; i < SIZE; i++)
        res[i]=0;

    for (int i = 0; i < SIZE; i++)
        for (int j = 0; j < SIZE; j++)
            res[i] +=  A[i][j] * a[j];
    
    for (int i = 0; i < SIZE; i++)
        printf("%.2f ",res[i]);

    printf("\n");
    printf("%.2f\n", somaVetor(res));
    return 0;
}
