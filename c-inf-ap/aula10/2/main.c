#include<stdlib.h>
#include<stdio.h>
#include<math.h>

int main(){
    int A[2][2], tr_A=0, det_A;
    for (int i = 0; i < 2; i++){
        for (int j = 0; j < 2; j++){
            printf("A[%d][%d]=", i, j);
            scanf("%d", &A[i][j]);
        }
    }
    
    printf("\n");
    for (int i = 0; i < 2; i++){
        for (int j = 0; j < 2; j++)
        {
            printf("%i ", A[i][j]);
            if (i==j) tr_A += A[i][j];
        }
        printf("\n");
    }

    printf("Traço de A = %d\n", tr_A);
    return 0;
}
