#include<stdlib.h>
#include<stdio.h>
#include<math.h>

#define WIDTH 3
#define HEIGHT 3

int main(){
    float B[HEIGHT][WIDTH]={
        {1,5,5},
        {5,5,5},
        {1,5,1}
    }; 
    int tr_B=0, det_B=0;

{
    // for (int i = 0; i < HEIGHT; i++){
    //     for (int j = 0; j < WIDTH; j++){
    //         printf("B[%d][%d]=", i, j);
    //         scanf("%f", &B[i][j]);
    //     }
    // }
}

    printf("\n");
    for (int i = 0; i < HEIGHT; i++){
        for (int j = 0; j < WIDTH; j++)
        {
            printf("%.2f ", B[i][j]);
            if (i==j) tr_B += B[i][j];
        }
        printf("\n");
    }

    for (int i = 0; i < WIDTH; i++)
    {
        float linP=1, linS=1;
        for (int j = 0; j < HEIGHT; j++)
        {
            linP*= B[j]       [(i+j)%WIDTH];
            linS*= B[HEIGHT-1-j][(i+j)%WIDTH];
        }
        det_B+=linP-linS;
    }
    
    printf("Det de B = %d\n", det_B);
    printf("Traço de B = %d\n", tr_B);
    return 0;
}
