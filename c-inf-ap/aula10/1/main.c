#include<stdlib.h>
#include<stdio.h>
#include<math.h>

int main(){
    float notas[2][5];
    for (int i = 0; i < 2; i++){
        for (int j = 0; j < 5; j++){
            printf("Digite a nota do aluno %d na materia %d: ", i, j);
            scanf("%f", &notas[i][j]);
        }
    }
    
    printf("\n\n\n\n\n\n\n");
    for (int i = 0; i < 2; i++){
        for (int j = 0; j < 5; j++){
            printf("%2.2f ", notas[i][j]);
        }
        printf("\n");
    }
    return 0;
}
